package utils

import (
	"errors"
	"log"
	"net/http"
)

func ValidateArgument(w http.ResponseWriter, request *http.Request, argumentName string) (string, error) {
	argument, ok := request.URL.Query()[argumentName]

	if !ok || len(argument) == 0 {
		log.Println("Missing URL parameter source: " + request.URL.RequestURI())
		SendResponse(w, RequestResponse{Success: false, Message: "Missing URL parameter " + argumentName})
		return "", errors.New("Missing parameter " + argumentName + ".")
	}

	return argument[0], nil
}
