package utils

import (
	"encoding/json"
	"net/http"
)

type RequestResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

func SendResponse(w http.ResponseWriter, response RequestResponse) {
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(response)
}
