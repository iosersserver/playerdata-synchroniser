package files

import (
	"io/ioutil"
	"playerdata-synchroniser/constants"
)

func CopyData(sourceServer, targetServer, uuid string) error {
	sourceFilePath := constants.ServerDirectories[sourceServer] + constants.WorldNames[sourceServer] + "/playerdata/" + uuid + ".dat"
	targetFilePath := constants.ServerDirectories[targetServer] + constants.WorldNames[targetServer] + "/playerdata/" + uuid + ".dat"

	return copyFile(sourceFilePath, targetFilePath)
}

func SaveData(sourceServer, uuid string) error {
	sourceFilePath := constants.ServerDirectories[sourceServer] + constants.WorldNames[sourceServer] + "/playerdata/" + uuid + ".dat"
	targetFilePath := constants.DataDirectory + uuid + ".dat"

	return copyFile(sourceFilePath, targetFilePath)
}

func LoadData(targetServer, uuid string) error {
	sourceFilePath := constants.DataDirectory + uuid + ".dat"
	targetFilePath := constants.ServerDirectories[targetServer] + constants.WorldNames[targetServer] + "/playerdata/" + uuid + ".dat"

	return copyFile(sourceFilePath, targetFilePath)
}

func copyFile(sourceFilePath, targetFilePath string) error {
	sourceFileContent, err := ioutil.ReadFile(sourceFilePath)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(targetFilePath, sourceFileContent, 0664)
	return err
}
