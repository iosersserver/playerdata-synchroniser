package constants

var (
	ServerDirectories = map[string]string{
		"plotwelt": "/home/das/Documents/Plotwelt/",
		"farmwelt": "/home/das/Documents/Farmwelt/",
	}

	WorldNames = map[string]string{
		"plotwelt": "world",
		"farmwelt": "world",
	}

	DataDirectory = "playerdata_saves/"
)
