package main

import (
	"log"
	"net/http"
	"os"
	"playerdata-synchroniser/constants"
	"playerdata-synchroniser/endpoints"
)

func main() {
	// Register endpoint handlers
	http.HandleFunc("/loadData", endpoints.LoadDataEndpoint)
	http.HandleFunc("/saveData", endpoints.SaveDataEndpoint)
	http.HandleFunc("/synchroniseData", endpoints.SynchroniseDataEndpoint)

	// Mkdir the root directory
	err := os.Mkdir(constants.DataDirectory, 0777)
	if os.IsNotExist(err) {
		panic(err)
	}

	// Start listening for incoming requests
	log.Println("Listening for incoming requests...")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
