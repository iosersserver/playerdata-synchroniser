package endpoints

import (
	"log"
	"net/http"
	"playerdata-synchroniser/files"
	"playerdata-synchroniser/utils"
	"strings"
)

func LoadDataEndpoint(w http.ResponseWriter, request *http.Request) {
	hostName := strings.Split(request.Host, ":")[0]
	if hostName != "localhost" {
		log.Println("Blocked connection from " + hostName)
		return
	}

	target, err := utils.ValidateArgument(w, request, "target")
	if err != nil {
		return
	}

	uuid, err := utils.ValidateArgument(w, request, "uuid")
	if err != nil {
		return
	}

	err = files.LoadData(target, uuid)
	if err != nil {
		utils.SendResponse(w, utils.RequestResponse{Success: false, Message: "Failed to copy data: " + err.Error()})
	} else {
		utils.SendResponse(w, utils.RequestResponse{Success: true, Message: uuid})
	}
}
