package endpoints

import (
	"log"
	"net/http"
	"playerdata-synchroniser/files"
	"playerdata-synchroniser/utils"
	"strings"
)

func SynchroniseDataEndpoint(w http.ResponseWriter, request *http.Request) {
	hostName := strings.Split(request.Host, ":")[0]
	if hostName != "localhost" {
		log.Println("Blocked connection from " + hostName)
		return
	}

	source, err := utils.ValidateArgument(w, request, "source")
	if err != nil {
		return
	}

	target, err := utils.ValidateArgument(w, request, "target")
	if err != nil {
		return
	}

	uuid, err := utils.ValidateArgument(w, request, "uuid")
	if err != nil {
		return
	}

	log.Println("Received valid request: " + request.URL.RequestURI())

	// Try to copy the player data file
	err = files.CopyData(source, target, uuid)
	if err != nil {
		utils.SendResponse(w, utils.RequestResponse{Success: false, Message: "Failed to copy data: " + err.Error()})
	} else {
		utils.SendResponse(w, utils.RequestResponse{Success: true, Message: uuid})
	}
}
